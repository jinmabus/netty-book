package me.kjjo.nettytest.ch3;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import me.kjjo.nettytest.ch2.EchoServerHandler;

public class EchoServer {
    public static void main(String[] args) {
        // 클라이언트 연결을 수락하는 부모 스레드 그룹
        // nThread: 스레드 그룹내에서 생성할 최대 스레드 수(1: 단일 스레드)
        // => 단일 스레드로 동작하는 NioEventLoopGroup객체로 생성
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        // 연결된 클라이언트의 소켓으로부터 데이터 입출력 및 이벤트 처리를 담당하는 자식 스레드 그룹
        // nThread: 미지정 -> cpu 코어 수를 기준으로 결정됨. cpu core * 2(ex: 4core cpu, 하이퍼스레딩 지원 => 16 스레드)
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try{
            // 부트스트랩 생성
            ServerBootstrap b = new ServerBootstrap();
            // group, channel 등 초기화
            //첫인자는 부모스레드(클라이언트 연결 요청 수락), 두번째인자는 연결된 소켓에 대한 i/o처리 담당하는 자식 스레드
            b.group(bossGroup, workerGroup)
                    //서버 소켓(부모 스레드)가 사용할 네트워크 입출력 모드 설정(NioServerSocketChannel, NIO모드로 동작)
                    .channel(NioServerSocketChannel.class)
                    //자식 채널의 초기화 방법 설정. 익명 클래스로 초기화
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        //ChannelInitializer는 클라이언트로부터 연결된 채널이 초기화될 때의 기본 동작이 지정된 추상 클래스임
                        @Override
                        public void initChannel(SocketChannel ch) {
                            // 채널 파이프라인 객체 생성
                            ChannelPipeline p = ch.pipeline();
                            // 채널 파이프라인에 EchoServerHandler 클래스 등록. EchoServerHandler는 클라이언트의 연결이 생성되었을 때 데이터 처리를 담당
                            p.addLast(new EchoServerHandler());
                        }
                    });

            ChannelFuture f = b.bind(8888).sync();

            f.channel().closeFuture().sync();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
}
