package me.kjjo.nettytest.ch3;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.oio.OioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.oio.OioServerSocketChannel;
import me.kjjo.nettytest.ch2.EchoServerHandler;

public class BlockingEchoServer {
    public static void main(String[] args) {
        //블로킹 모드로 변경하기 위해서 OioEventLoopGroup을 사용(OIO는 Old-Blocking-IO의 약자)
        EventLoopGroup bossGroup = new OioEventLoopGroup(1);
        //블로킹 모드로 변경하기 위해서 OioEventLoopGroup을 사용
        EventLoopGroup workerGroup = new OioEventLoopGroup();

        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    //블로킹 모드로 변경하기 위해서 OioServerSocketChannel 사용
                    //사용가능한 소켓채널: 블로킹, 논블로킹, epoll(epool은 리눅스 전용)
                    .channel(OioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) {
                            ChannelPipeline p = ch.pipeline();
                            p.addLast(new EchoServerHandler());
                        }
                    });
        }
        finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
}
