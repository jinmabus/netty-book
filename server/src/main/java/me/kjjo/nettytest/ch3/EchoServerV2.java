package me.kjjo.nettytest.ch3;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import me.kjjo.nettytest.ch2.EchoServerHandler;

public class EchoServerV2 {
    public static void main(String[] args) {
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try{
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        //ChannelInitializer는 클라이언트로부터 연결된 채널이 초기화될 때의 기본 동작이 지정된 추상 클래스임
                        @Override
                        public void initChannel(SocketChannel ch) {
                            ChannelPipeline p = ch.pipeline();
                            p.addLast(new EchoServerHandler());
                        }
                    });

            ChannelFuture f = b.bind(8888).sync();

            f.channel().closeFuture().sync();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
}


/*


Jul 14, 2018 8:35:10 PM io.netty.handler.logging.LoggingHandler channelRegistered
정보: [id: 0xe88510d7] REGISTERED
Jul 14, 2018 8:35:10 PM io.netty.handler.logging.LoggingHandler bind
정보: [id: 0xe88510d7] BIND(0.0.0.0/0.0.0.0:8888)
Jul 14, 2018 8:35:10 PM io.netty.handler.logging.LoggingHandler channelActive
정보: [id: 0xe88510d7, /0:0:0:0:0:0:0:0:8888] ACTIVE ===> 서버 실행시 여기까지 출력됨. 8888포트에 바인드되고 8888포트가 활성화 되었음을 의미



Jul 14, 2018 8:36:01 PM io.netty.handler.logging.LoggingHandler logMessage
정보: [id: 0xe88510d7, /0:0:0:0:0:0:0:0:8888] RECEIVED: [id: 0xefb72c26, /0:0:0:0:0:0:0:1:50202 => /0:0:0:0:0:0:0:1:8888]
        ==> telnet 으로 접속하면 이 메시지까지 출력됨. 0:0:0:0:0:0:0:1 ip로 50202포트로 동작중인 서버의 0:0:0:0:0:0:0:1 8888포트에 접속 했음을 의미???



 */