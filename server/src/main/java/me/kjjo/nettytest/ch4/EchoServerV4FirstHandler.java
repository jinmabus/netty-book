package me.kjjo.nettytest.ch4;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.nio.charset.Charset;

public class EchoServerV4FirstHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        ByteBuf readMessage = (ByteBuf) msg;
        System.out.println("FirstHandler channelRead : "+readMessage.toString(Charset.defaultCharset()));

        ctx.write(msg);

        ctx.fireChannelRead(msg);   //채널 파이프라인에 channelRead이벤트를 발생시킨다 -> 다음 등록된 handler가 받아서 처리할 수 있다
    }
}
