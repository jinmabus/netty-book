package me.kjjo.nettytest.ch4;

import io.netty.buffer.ByteBuf;
import io.netty.channel.*;

import java.net.SocketAddress;
import java.nio.charset.Charset;

public class EchoServerV2TestOutboundHandler extends ChannelOutboundHandlerAdapter {

    @Override
    public void bind(ChannelHandlerContext ctx, SocketAddress localAddress,
                     ChannelPromise promise) throws Exception {
        ctx.bind(localAddress, promise);
        //super.bind(localAddress, promise);

        System.out.println("bind");
    }



    @Override
    public void connect(ChannelHandlerContext ctx, SocketAddress remoteAddress,
                        SocketAddress localAddress, ChannelPromise promise) throws Exception {

        System.out.println("connect");
        ctx.connect(remoteAddress, localAddress, promise);
    }


    @Override
    public void disconnect(ChannelHandlerContext ctx, ChannelPromise promise)
            throws Exception {
        System.out.println("disconnect");
        ctx.disconnect(promise);
    }


    @Override
    public void close(ChannelHandlerContext ctx, ChannelPromise promise)
            throws Exception {
        System.out.println("close");
        ctx.close(promise);
    }


    @Override
    public void deregister(ChannelHandlerContext ctx, ChannelPromise promise) throws Exception {
        System.out.println("deregister");
        ctx.deregister(promise);
    }


    @Override
    public void read(ChannelHandlerContext ctx) throws Exception {
        System.out.println("read");
        ctx.read();
    }


    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        System.out.println("write");
        ctx.write(msg, promise);
    }


    @Override
    public void flush(ChannelHandlerContext ctx) throws Exception {
        System.out.println("flush");

        ctx.flush();
    }



}
