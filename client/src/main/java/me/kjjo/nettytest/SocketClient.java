package me.kjjo.nettytest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketClient {

    public static void main(String[] args) throws IOException {
        System.out.println("Hello World!");

        Socket echoSock = new Socket("localhost", 8888);

        PrintWriter out = new PrintWriter(echoSock.getOutputStream(), false);
        BufferedReader in = new BufferedReader(new InputStreamReader(echoSock.getInputStream()));
        //BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));

        out.write(11111);
        out.flush();

        int a = in.read();
        System.out.println("received="+a);

        echoSock.close();

        System.out.println("closed");

    }
}
